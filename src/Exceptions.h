//
// Created by Tobias Winkler on 25.10.21.
//

#ifndef PRODIGY_EXCEPTIONS_H
#define PRODIGY_EXCEPTIONS_H

#include <stdexcept>

namespace prodigy {

    class not_implemented_error : std::exception {};

    class bad_input_error : std::exception {};

}

#endif //PRODIGY_EXCEPTIONS_H
